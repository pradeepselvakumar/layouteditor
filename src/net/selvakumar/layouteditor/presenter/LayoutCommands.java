package net.selvakumar.layouteditor.presenter;

import net.selvakumar.layouteditor.model.ILayoutEditorModel;
import net.selvakumar.layouteditor.model.IShelf;
import java.awt.Point;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

class AddShelfCommand extends AbstractUndoableEdit {
    private IShelf _shelf;
    private final Point _location;
    private final ILayoutEditorModel _model;

    public AddShelfCommand(ILayoutEditorModel model, Point location) {
        _model = model;
        _location = location;
    }

    public boolean execute() {
        _shelf = _model.addShelf(_location);
        return true;
    }
    
    public void undo() throws CannotUndoException {
        super.undo();
        _model.removeShelf(_shelf);
    }

    public void redo() throws CannotRedoException {
        super.redo();
        _model.addShelf(_shelf);
    }
}

class DeleteSelectedShelvesCommand extends AbstractUndoableEdit {
    private HashSet<IShelf> _selectedShelves;
    private final ILayoutEditorModel _model;
    private final ILayoutEditorPresenter _presenter;

    public DeleteSelectedShelvesCommand(ILayoutEditorPresenter presenter, ILayoutEditorModel model, HashSet<IShelf> selectedShelves) {
        _presenter = presenter;
        _model = model;
        _selectedShelves = new HashSet<IShelf>(selectedShelves);
    }

    public boolean execute() {
        _model.removeShelves(_selectedShelves);
        return true;
    }
    
    public void undo() throws CannotUndoException {
        super.undo();
        
        _model.addShelves(_selectedShelves);
        _presenter.getSelectedShelves().addAll(_selectedShelves);
    }

    public void redo() throws CannotRedoException {
        super.redo();
        _model.removeShelves(_selectedShelves);
    }
}

class MoveSelectedShelvesCommand extends AbstractUndoableEdit {
    
    private HashMap<IShelf, Point> _originalLocations;
    private HashMap<IShelf, Point> _newLocations;
    
    private final ILayoutEditorModel _model;
    private final ILayoutEditorPresenter _presenter;


    MoveSelectedShelvesCommand(LayoutEditorPresenter presenter, ILayoutEditorModel model) {
        _presenter = presenter;
        _model = model;

        _originalLocations = new HashMap<>();
        Collection<IShelf> selectedShelves = presenter.getSelectedShelves();
        for (IShelf s : selectedShelves) {
            _originalLocations.put(s, s.getLocation());
        }
    }

    public boolean execute() {
        _newLocations = new HashMap<>();

        for(IShelf s : _presenter.getSelectedShelves()) {
            // this will coerce the locations to valid grid locations
            _model.setShelfLocation(s, s.getLocation());
            _newLocations.put(s, s.getLocation());
        }

        return true;
    }
    
    public void undo() throws CannotUndoException {
        super.undo();

        for (IShelf s : _originalLocations.keySet()) {
            _model.setShelfLocation(s, _originalLocations.get(s));
        }
        
        _presenter.selectShelves(_originalLocations.keySet());
    }

    public void redo() throws CannotRedoException {
        super.redo();
        
        for (IShelf s : _newLocations.keySet()) {
            _model.setShelfLocation(s, _newLocations.get(s));
        }

        _presenter.selectShelves(_newLocations.keySet());
    }
}