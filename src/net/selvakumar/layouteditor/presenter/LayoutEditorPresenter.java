/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.presenter;

import com.google.gson.JsonSyntaxException;
import java.awt.KeyEventDispatcher;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import javax.swing.JOptionPane;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;
import net.selvakumar.layouteditor.model.GsonOperationsProvider;
import net.selvakumar.layouteditor.model.IGsonOperationsProvider;
import net.selvakumar.layouteditor.model.ILayoutEditorModel;
import net.selvakumar.layouteditor.model.IShelf;
import net.selvakumar.layouteditor.model.LayoutModelSerializable;
import net.selvakumar.layouteditor.view.ILayoutEditorView;


/**
 *
 * @author Pradeep Selvakumar
 */

public class LayoutEditorPresenter implements ILayoutEditorPresenter {
    private ILayoutEditorView _view;
    private ILayoutEditorModel _model;

    private MouseAdapter _mouseAdapter = new LayoutMouseAdapter();
    private LayoutKeyEventManager _keyEventDispatcher = new LayoutKeyEventManager();

    private ExtendedUndoManager _undoManager = new ExtendedUndoManager();

    private EditorStatuses _currentEditorStatus;
    private HashSet<IShelf> _selectedShelves = new HashSet<>();
    private MoveSelectedShelvesCommand _moveCmd;
    private IGsonOperationsProvider _gsonOpsProvider;

    public EditorStatuses getCurrentEditorStatus() {
        return _currentEditorStatus;
    }

    public IGsonOperationsProvider getGsonOperationsProvider() {
        if (_gsonOpsProvider == null)
            _gsonOpsProvider = new GsonOperationsProvider();

        return _gsonOpsProvider;
    }

    public LayoutEditorPresenter(ILayoutEditorView view, ILayoutEditorModel model) {
        _view = view;
        _model = model;

        if (_view != null)
        {
            _view.showEditor();
            _view.setViewEventsListener(this);
        }

        if (_model != null) {
            _model.setModelEventsListener(this);
        }

        _gsonOpsProvider = new GsonOperationsProvider();
        setNormalMode();
    }

    @Override
    public void fileNew() {
        boolean continueFileNew = askToSaveIfNeeded();
        
        if (!continueFileNew)
            return;
        
        
    }

    @Override
    public void fileOpen() {
        boolean continueFileOpen = askToSaveIfNeeded();

        // if the user closed the Save dialog, or clicked cancel
        // then don't continue with the save operation
        if (!continueFileOpen)
            return;

        String fileName = _view.showFileOpenDialog();

        if (fileName.isEmpty())
            return;

        LayoutModelSerializable model = null;

        try {
            model = _gsonOpsProvider.Open(fileName);
            _model.getShelves().clear();

            _undoManager.discardAllEdits();
            _undoManager.resetUndoableActionsCount();

            _model.addShelves(model.getShelves());
            _model.setFileName(fileName);
            _view.setWindowTitle(fileName);
        } catch(JsonSyntaxException ex) {
            _view.showErrorDialog(EditorMessages.INCORRECT_FILE_FORMAT, ex);
        } catch (Exception ex) {
            _view.showErrorDialog(EditorMessages.ERROR_OPENING_FILE, ex);
        } finally {
            _view.redraw();
        }
    }

    @Override
    public void fileSave() {
        String fileName = _model.getFileName();

        if (fileName.isEmpty()) {
            fileName = _view.showFileSaveDialog();
            _model.setFileName(fileName);
        }

        if (fileName.isEmpty())
            return;

        try {
            _gsonOpsProvider.SaveAs(_model.getSerializableModel(), fileName);
            _model.setFileName(fileName);
            _undoManager.resetUndoableActionsCount();
        } catch (IOException ex) {
            _view.showErrorDialog(EditorMessages.ERROR_SAVING_FILE, ex);
        }
    }

    @Override
    public void newShelf() {
        _view.showStatus(LayoutEditorPresenter.StatusMessages.CLICK_TO_ADD_SHELF);

        _currentEditorStatus = EditorStatuses.AddShelf;
    }

    @Override
    public void setNormalMode() {
        _currentEditorStatus = EditorStatuses.Normal;
    }

    @Override
    public void shelvesCollectionChanged() {
        _view.setShelves(_model.getShelves());
        clearSelectedShelves();
        _view.setSelectedShelves(_selectedShelves);
        _view.redraw();
    }

    @Override
    public Collection<IShelf> getSelectedShelves() {
        return _selectedShelves;
    }

    @Override
    public void undo() {

        if (_undoManager.canUndo()) {
            _undoManager.undo();
            _view.redraw();
        }
    }

    @Override
    public void redo() {
         if (_undoManager.canRedo()) {
            _undoManager.redo();
            _view.redraw();
        }
    }

    @Override
    public void selectShelves(Collection<IShelf> selectedShelves) {
        _selectedShelves.addAll(selectedShelves);
        selectedShelvesCollectionChanged();
    }

    @Override
    public MouseAdapter getMouseHandler() {
        return _mouseAdapter;
    }

    @Override
    public KeyEventDispatcher getKeyEventsHandler() {
        return _keyEventDispatcher;
    }

    @Override
    public void setGsonOperationsProvider(IGsonOperationsProvider provider) {
        _gsonOpsProvider = provider;
    }

    void setUndoManager(ExtendedUndoManager undoManager) {
        _undoManager = undoManager;
    }

    private void showDefaultCursor() {
        _view.showDefaultCursor();
    }

    private void updateCursor(Point pt) {
        switch(_currentEditorStatus) {
            case AddShelf:
                _view.showCrosshair();
                break;
            case Normal:
                IShelf shelf = getShelfAtPoint(pt);
                if (shelf != null) {
                    _view.showDragDropCursor();
                } else {
                    _view.showDefaultCursor();
                }
                break;
            default:
                _view.showDefaultCursor();
                break;
        }
    }

    private void selectShelfAt(Point pt) {
        IShelf shelf = getShelfAtPoint(pt);

        if (shelf == null || !_selectedShelves.contains(shelf)) {
            clearSelectedShelves();
        }

        if (shelf != null) {
            _selectedShelves.add(shelf);
        }

        selectedShelvesCollectionChanged();
    }

    private IShelf getShelfAtPoint(Point pt) {
        if (_model.getShelves() == null)
            return null;    // this should only happen during unit tests.

        for(IShelf shelf : _model.getShelves()) {
            Point location = shelf.getLocation();

            //TODO: the size of a ahelf is hard coded for now
            if (new Rectangle2D.Double(location.x, location.y, 25, 50).contains(pt)) {
                return shelf;
            }
        }

        return null;
    }

    private void discreteSelectShelfAt(Point pt) {
        IShelf shelf = getShelfAtPoint(pt);

        if (shelf != null) {
            if (!_selectedShelves.contains(shelf)) {
                _selectedShelves.add(shelf);
            } else {
                _selectedShelves.remove(shelf);
            }
        } else {
            _selectedShelves.clear();
        }

        selectedShelvesCollectionChanged();
    }

    private void offsetSelectedShelves(int offsetX, int offsetY) {
        if (_selectedShelves.size() == 0)
            return;

        if (_currentEditorStatus != EditorStatuses.DraggingShelves) {
            _moveCmd = new MoveSelectedShelvesCommand(this, _model);

            _currentEditorStatus = EditorStatuses.DraggingShelves;
        }

        for (IShelf s : _selectedShelves) {
            Point location = s.getLocation();
            s.setLocation(new Point(location.x + offsetX, location.y + offsetY));
        }

        _view.setShelves(_model.getShelves());
        selectedShelvesCollectionChanged();
    }

    private void addNewShelf(Point pt) {
        AddShelfCommand addCmd = new AddShelfCommand(_model, pt);
        if (addCmd.execute())
            _undoManager.addEdit(addCmd);
    }

    private void finishDraggingShelves(Point pt) {
        _currentEditorStatus = EditorStatuses.Normal;

        if (_moveCmd.execute())
            _undoManager.addEdit(_moveCmd);

        _view.redraw();
    }

    private void selectedShelvesCollectionChanged() {
        _view.setSelectedShelves(_selectedShelves);
        _view.redraw();
    }

    private boolean askToSaveIfNeeded() {
        if (_undoManager.getUndoableActionsCount() == 0)
            return true;

        boolean canceled = false;
        int shouldSave = _view.showSaveDialog(EditorMessages.SHOULD_SAVE);
        switch(shouldSave) {
            case JOptionPane.YES_OPTION:
                fileSave();
                break;
            case JOptionPane.CANCEL_OPTION:
            case JOptionPane.CLOSED_OPTION:
                canceled = true;
            case JOptionPane.NO_OPTION:
            default:
                break;
        }

        return !canceled;
    }

    private void clearSelectedShelves() {
        _selectedShelves.clear();
    }

    private void deleteSelectedShelves() {
        if (_selectedShelves.size() == 0)
            return;

        DeleteSelectedShelvesCommand deleteCmd = new DeleteSelectedShelvesCommand(this, _model, _selectedShelves);

        if (deleteCmd.execute())
            _undoManager.addEdit(deleteCmd);

        clearSelectedShelves();
    }

    @Override
    public void onClosing() {
        if (askToSaveIfNeeded())
            _view.Close();
    }

    // TODO: missing unit tests
    class LayoutMouseAdapter extends MouseAdapter {
        private Point _startLocation;

        @Override
        public void mouseEntered(MouseEvent me) {
            updateCursor(me.getPoint());
        }

        @Override
        public void mouseExited(MouseEvent me) {
            showDefaultCursor();
        }

        @Override
        public void mouseMoved(MouseEvent me) {
            updateCursor(me.getPoint());
        }

        @Override
        public void mousePressed(MouseEvent me) {
            if (_currentEditorStatus != EditorStatuses.Normal)
                return;

            Point pt = me.getPoint();
            _startLocation = pt;

            if (me.isControlDown()) {
                discreteSelectShelfAt(pt);
            } else {
                selectShelfAt(pt);
            }
        }

        @Override
        public void mouseDragged(MouseEvent me) {
            if (_currentEditorStatus == EditorStatuses.AddShelf)
                return;

            Point location = me.getPoint();
            int dx = location.x - _startLocation.x;
            int dy = location.y - _startLocation.y;

            offsetSelectedShelves(dx, dy);
            _startLocation = location;
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            _startLocation = null;
            Point pt = me.getPoint();

            if (_currentEditorStatus == EditorStatuses.AddShelf) {
                addNewShelf(pt);
            } else if (_currentEditorStatus == EditorStatuses.DraggingShelves) {
                finishDraggingShelves(pt);
            } else if (!me.isControlDown()) {
                selectShelfAt(pt);
            }
        }
    }

    class LayoutKeyEventManager implements KeyEventDispatcher {
        @Override
        public boolean dispatchKeyEvent(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_DELETE) {
                deleteSelectedShelves();
            }
            return false;
        }

    }

    public interface StatusMessages {
        public final static String CLICK_TO_ADD_SHELF = "Click to add Shelf";
    }

    public enum EditorStatuses {
        Normal,
        AddShelf,
        DraggingShelves
    }

    public interface EditorMessages {
        public final static String ERROR_SAVING_FILE = "An error occurred when attempting to save";
        public final static String ERROR_OPENING_FILE = "An error occurred when attempting to open";
        public final static String INCORRECT_FILE_FORMAT = "The file is not in the correct format.";

        public final static String SHOULD_SAVE = "Would you like to save your changes?";
    }
}
class ExtendedUndoManager extends UndoManager {
    private int _undoableActionsCount = 0;
    
    public void resetUndoableActionsCount() {
        _undoableActionsCount = 0;
    }
    
    public int getUndoableActionsCount() {
        return _undoableActionsCount;
    }

    @Override
    public boolean addEdit(UndoableEdit edit) {
        _undoableActionsCount++;
        return super.addEdit(edit);
    }
    
    @Override
    public void undo() {
        _undoableActionsCount--;
        super.undo();
    }
    
    @Override
    public void redo() {
        _undoableActionsCount++;
    }
    
}