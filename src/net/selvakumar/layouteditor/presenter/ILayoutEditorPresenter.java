package net.selvakumar.layouteditor.presenter;

import java.util.Collection;
import net.selvakumar.layouteditor.model.IGsonOperationsProvider;
import net.selvakumar.layouteditor.model.IModelEventsListener;
import net.selvakumar.layouteditor.model.IShelf;
import net.selvakumar.layouteditor.view.IViewEventsListener;

public interface ILayoutEditorPresenter extends IViewEventsListener, IModelEventsListener {
    public Collection<IShelf> getSelectedShelves();

    public void selectShelves(Collection<IShelf> _selectedShelves);

    public void setGsonOperationsProvider(IGsonOperationsProvider mockProvider);
}

