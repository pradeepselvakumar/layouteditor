/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import net.selvakumar.layouteditor.model.IShelf;


/**
 *
 * @author pselvakumar
 */
class EditorPanel extends JPanel {
    private Collection<IShelf> _shelves;

    private final int GRID_SIZE = 50;

    Image _shelfImage;
    private Collection<IShelf>  _selectedShelves;

    public EditorPanel() {
        _shelfImage = new ImageIcon(getClass().getResource("/resources/shelf_1_25x50.png")).getImage();
        _shelves = new ArrayList<IShelf>();
        _selectedShelves = new ArrayList<IShelf>();

        setFocusable(true);
    }

    public void setShelves(Collection<IShelf> shelves) {
        _shelves = shelves;
    }

    public void setSelectedShelves(Collection<IShelf> shelves) {
        _selectedShelves = shelves;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);

        drawGrid(g2d);

    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(1200, 800);
    }

    private void drawGrid(Graphics2D g2d) {
        double w = getWidth() - 1;
        double h = getHeight() - 1 ;

        Rectangle2D.Double rect = new Rectangle2D.Double(0, 0, w, h);

        g2d.setColor(new Color(245, 255, 246));
        g2d.fill(rect);

        g2d.setColor(new Color(181, 218, 249));
        // draw vertical lines
        for (int i = GRID_SIZE; i < w; i += GRID_SIZE) {
            Line2D.Double line = new Line2D.Double(rect.x + i, rect.y, rect.x + i, rect.y + rect.height);
            g2d.draw(line);
        }

        // draw horizontal lines
        for (int i = GRID_SIZE; i < rect.height; i += GRID_SIZE) {
            Line2D.Double line = new Line2D.Double(rect.x, rect.y + i, rect.x + rect.width, rect.y + i);
            g2d.draw(line);
        }

        for (IShelf shelf : _shelves) {
            Point location = shelf.getLocation();
            g2d.drawImage(_shelfImage, location.x, location.y, this);
        }

        for (IShelf shelf : _selectedShelves)  {
            Point p = shelf.getLocation();
            g2d.drawRect(p.x, p.y, 5, 5);
        }

        g2d.setColor(new Color(77, 77, 77));
        g2d.draw(rect);
    }


    private void drawIsometricGrid(Graphics2D g2d) {
        double w = getWidth();
        double h = getHeight();

        Rectangle2D.Double rect = new Rectangle2D.Double(100, 100, w - 200, h - 200);

        AffineTransform at = new AffineTransform();
        at.scale(1.0, 0.5);
        at.rotate(Math.PI/4,
                rect.x + rect.width / 2, rect.y + rect.height / 2);
        at.translate(rect.width / 2 - 200, rect.height / 2);

        Shape transformedRect = at.createTransformedShape(rect);
        g2d.draw(transformedRect);
        g2d.setColor(Color.LIGHT_GRAY);
        g2d.fill(transformedRect);

        g2d.setColor(Color.darkGray);
        // draw vertical lines
        for (int i = 24; i < rect.width; i += 24) {
            Line2D.Double line = new Line2D.Double(rect.x + i, rect.y, rect.x + i, rect.y + rect.height);
            g2d.draw(at.createTransformedShape(line));
        }

        // draw horizontal lines
        for (int i = 24; i < rect.height; i += 24) {
            Line2D.Double line = new Line2D.Double(rect.x, rect.y + i, rect.x + rect.width, rect.y + i);
            g2d.draw(at.createTransformedShape(line));
        }

         for (IShelf shelf : _shelves) {
             at = new AffineTransform();
             at.scale(1.0, 0.5);
             at.rotate(Math.PI/4,
                rect.x + rect.width / 2, rect.y + rect.height / 2);
             Point p = shelf.getLocation();
             at.translate(p.x + rect.width / 2 - 200, p.y + rect.height / 2);
            //g2d.drawRenderedImage(_shelf1, at);
        }
    }
}
