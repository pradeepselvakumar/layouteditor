/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.view;

import java.util.Collection;
import net.selvakumar.layouteditor.model.IShelf;

public interface ILayoutEditorView {
    public void showEditor();

    public void showCrosshair();

    public void showDefaultCursor();

    public void setViewEventsListener(IViewEventsListener listener);

    public void showStatus(String message);

    public void redraw();

    public void showDragDropCursor();

    public void setShelves(Collection<IShelf> shelves);

    public void setSelectedShelves(Collection<IShelf> shelves);

    public String showFileOpenDialog();

    public String showFileSaveDialog();

    public void showErrorDialog(String errorMessage, Exception exception);

    public void setWindowTitle(String fileName);

    public int showSaveDialog(String message);

    public void Close();

}

