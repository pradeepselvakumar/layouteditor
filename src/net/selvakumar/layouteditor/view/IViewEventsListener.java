package net.selvakumar.layouteditor.view;

import java.awt.KeyEventDispatcher;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowListener;

public interface IViewEventsListener {
    public void fileNew();
    public void fileOpen();
    public void fileSave();
    
    public void newShelf();
    public void setNormalMode();
    
    public void undo();
    public void redo();

    public MouseAdapter getMouseHandler();

    public KeyEventDispatcher getKeyEventsHandler();

    public void onClosing();
}
