package net.selvakumar.layouteditor.view;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.GridBagLayout;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;
import net.selvakumar.layouteditor.model.IShelf;

public class LayoutEditorView extends JFrame implements ILayoutEditorView {

    EditorPanel _editPanel;
    JLabel _statusbar;
    JToolBar _toolbar;

    IViewEventsListener _viewEventsListener;
    private final static String APP_NAME = "Layout Editor";
    private JFileChooser _chooser = new JFileChooser();

    public LayoutEditorView() {
        initUI();
    }

    public final void initUI() {
        setupToolbar();

        _editPanel = new EditorPanel();
        _editPanel.setPreferredSize(new Dimension(1200, 800));

        JPanel backgroundPanel = new JPanel(new GridBagLayout());
        backgroundPanel.add(_editPanel);

        JScrollPane scrollPane = new JScrollPane(backgroundPanel);
        add(scrollPane, BorderLayout.CENTER);

        _statusbar = new JLabel(" Statusbar");
        _statusbar.setPreferredSize(new Dimension(-1, 22));
        _statusbar.setBorder(LineBorder.createGrayLineBorder());
        add(_statusbar, BorderLayout.SOUTH);

        setWindowTitle("");

        // todo need to set minimum size
        setSize(1200, 800); // will be nice to save and restore the last known size

        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
    }

    @Override
    public void showEditor() {
        setVisible(true);
        redraw();
        showStatus(_editPanel.getSize().toString());
    }

    @Override
    public void showCrosshair() {
        if (getCursor().getType() != Cursor.CROSSHAIR_CURSOR) {
            setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        }
    }

    @Override
    public void showDefaultCursor() {
        if (getCursor().getType() != Cursor.DEFAULT_CURSOR) {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    
    @Override
    public void setViewEventsListener(final IViewEventsListener listener) {
        _viewEventsListener = listener;
        addMouseHandler(_viewEventsListener.getMouseHandler());
        addKeyHandler(_viewEventsListener.getKeyEventsHandler());

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                listener.onClosing();
            }
        });
    }

    @Override
    public void showStatus(String message) {
        _statusbar.setText(message);;
    }

    @Override
    public void redraw() {
        _editPanel.repaint();
    }

    @Override
    public void showDragDropCursor() {
        if (getCursor().getType() != Cursor.MOVE_CURSOR) {
            setCursor(new Cursor(Cursor.MOVE_CURSOR));
        }
    }

    @Override
    public void setShelves(Collection<IShelf> shelves) {
        _editPanel.setShelves(shelves);
    }

    @Override
    public void setSelectedShelves(Collection<IShelf> shelves) {
        _editPanel.setSelectedShelves(shelves);
    }

    @Override
    public String showFileOpenDialog() {
        // TODO: beef this up to filter by extension, and
        // figure out how to remember the last visited folder, and
        // to use current file's folder
        String fileName = "";
        if (_chooser.showOpenDialog(this) ==  JFileChooser.APPROVE_OPTION )  {
            fileName = _chooser.getSelectedFile().getAbsolutePath();
        }

        return fileName;
    }


    @Override
    public String showFileSaveDialog() {
        String fileName = "";

        if (_chooser.showSaveDialog(this) ==  JFileChooser.APPROVE_OPTION )  {
            fileName = _chooser.getSelectedFile().getAbsolutePath();
        }

        return fileName;
    }

    @Override
    public void showErrorDialog(String errorMessage, Exception exception) {
        JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
        // todo: optionally show the exception
    }

    @Override
    public void setWindowTitle(String fileName) {
        String title = APP_NAME;

        if (!fileName.isEmpty())
            title = String.format("%s - %s", APP_NAME, fileName);

        setTitle(title);
    }

    @Override
    public int showSaveDialog(String msg) {
        int n = JOptionPane.showOptionDialog(this,
            msg,
            "Save",
            JOptionPane.YES_NO_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            null,
            null);

        return n;
    }

    private void addMouseHandler(MouseAdapter layoutMouseAdapter) {
        _editPanel.addMouseListener(layoutMouseAdapter);
        _editPanel.addMouseMotionListener(layoutMouseAdapter);
    }

    private void addKeyHandler(KeyEventDispatcher dispatcher) {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(dispatcher);
    }


    private void setupToolbar() {
        _toolbar = new JToolBar();

        ToolbarActionManager toolbarActionManager = new ToolbarActionManager();

        addButtonToToolbar(EditorCommands.NewFile,
                "/resources/new_file_24x24.png",
                KeyEvent.VK_N,
                "Create a new layout (CTRL+N)",
                toolbarActionManager.NewFileAction());

        addButtonToToolbar(EditorCommands.OpenFile,
                "/resources/open_file_24x24.png",
                KeyEvent.VK_O,
                "Open a layout file (CTRL+O)",
                toolbarActionManager.OpenFileAction());

        addButtonToToolbar(EditorCommands.SaveFile,
                "/resources/save_file_24x24.png",
                KeyEvent.VK_S,
                "Save current layout to a file (CTRL+S)",
                toolbarActionManager.SaveFileAction());

        _toolbar.addSeparator();

        addButtonToToolbar(EditorCommands.Undo,
                "/resources/undo_24x24.png",
                KeyEvent.VK_Z,
                "Undo the last action (CTRL+Z)",
                toolbarActionManager.UndoAction());

        addButtonToToolbar(EditorCommands.Redo,
                "/resources/redo_24x24.png",
                KeyEvent.VK_Y,
                "Redo the last undo action  (CTRL+Y)",
                toolbarActionManager.RedoAction());

        _toolbar.addSeparator();

        addButtonToToolbar(EditorCommands.NewShelf,
                "/resources/shelf_3_24x24.png",
                KeyEvent.VK_W,
                "Add new shelves (CTRL+H)",
                toolbarActionManager.NewShelfAction());

        addButtonToToolbar(EditorCommands.Normal,
                "/resources/mouse_pointer_24x24.png",
                KeyEvent.VK_D,
                "Reset mouse cursor to enable drag drop operations (CTRL+D)",
                toolbarActionManager.NormalAction());

        add(_toolbar, BorderLayout.NORTH);
    }

    private void addButtonToToolbar(String cmd, String resourcePath, int keyboardShortcut, String tooltip, Action action) {
        JButton toolbarButton = new JButton(action);

        ImageIcon icon = new ImageIcon(getClass().getResource(resourcePath));
        toolbarButton.setIcon(icon);

        KeyStroke keyStroke = KeyStroke.getKeyStroke(keyboardShortcut, Event.CTRL_MASK);
        toolbarButton.getActionMap().put(cmd, action);
        toolbarButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyStroke, cmd);

        toolbarButton.setActionCommand(cmd);
        toolbarButton.setToolTipText(tooltip);

        _toolbar.add(toolbarButton);
    }

    void showMessage(String msg) {
        JOptionPane.showMessageDialog(null, msg, null, JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void Close() {
        dispose();
    }

    class ToolbarActionManager  {
        public Action NewFileAction () {
            return new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    _viewEventsListener.fileNew();
                }
            };
        }

        public Action OpenFileAction () {
            return new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    _viewEventsListener.fileOpen();
                }
            };
        }

        public Action SaveFileAction () {
            return new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    _viewEventsListener.fileSave();
                }
            };
        }

        public Action NewShelfAction () {
            return new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    _viewEventsListener.newShelf();
                }
            };
        }

        public Action UndoAction () {
            return new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                        _viewEventsListener.undo();
                }
            };
        }

        public Action RedoAction () {
            return new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    _viewEventsListener.redo();
                }
            };
        }

        public Action NormalAction () {
            return new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    _viewEventsListener.setNormalMode();
                }
            };
        }
    }

    class EditorCommands {
        public final static String NewFile = "New File";
        public final static String OpenFile = "Open File";
        public final static String SaveFile = "Save File";
        public final static String NewShelf = "New Shelf";
        public final static String Normal = "Normal";
        public final static String Undo = "Undo";
        public final static String Redo = "Redo";
    }
}