/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.model;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author pselvakumar
 */
public class LayoutModelSerializable {
    private Collection<IShelf> _shelves;

    public LayoutModelSerializable(ILayoutEditorModel model) {
        _shelves = model.getShelves();
    }

    public Collection<IShelf> getShelves() {
        return _shelves;
    }
}