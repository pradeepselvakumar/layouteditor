/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.model;

import java.awt.Point;

/**
 *
 * @author deeps
 */

public interface IShelf {
    public Point getLocation();

    public void setLocation(Point pt);
}
