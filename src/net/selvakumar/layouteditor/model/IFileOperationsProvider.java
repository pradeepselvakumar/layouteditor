/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.model;

import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author deeps
 */
public interface IFileOperationsProvider {

    public Writer getFileWriter(String fileName) throws IOException ;
    
}
