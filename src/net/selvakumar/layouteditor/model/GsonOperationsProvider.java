/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.selvakumar.layouteditor.presenter.LayoutEditorPresenter;

/**
 *
 * @author deeps
 */
public class GsonOperationsProvider implements IGsonOperationsProvider {

    IFileOperationsProvider _fileOperationsProvider;

    IFileOperationsProvider getFileOperationsProvider() {
        if (_fileOperationsProvider == null)
            _fileOperationsProvider = new FileOperationsProvider();

        return _fileOperationsProvider;
    }
    
    @Override
    public void setFileOperationsProvider(IFileOperationsProvider provider) {
        _fileOperationsProvider = provider;
    }

    @Override
    public void SaveAs(LayoutModelSerializable serializableModel, String fileName)
            throws IOException {
        try (JsonWriter writer = getJsonWriter(fileName)) {
            toJson(serializableModel, writer);
        }
        
    }
    
    JsonWriter getJsonWriter(String fileName) throws IOException {
        Writer writer;

        writer = getFileOperationsProvider().getFileWriter(fileName);

        return new JsonWriter(writer);
    }

    void toJson(LayoutModelSerializable serializableModel, JsonWriter writer) {
        Gson gson = new Gson();
        gson.toJson(gson.toJsonTree(serializableModel), writer);
    }

    @Override
    public LayoutModelSerializable Open(String filePath) 
            throws IOException {

        final Gson gson = new GsonBuilder().registerTypeAdapter(IShelf.class, new ShelfDeserializer()).create();

        try (JsonReader reader = new JsonReader(new FileReader(filePath))) {
            return gson.fromJson(reader, LayoutModelSerializable.class);
        }

    }
}


class ShelfDeserializer implements JsonDeserializer<IShelf> {
    @Override
    public IShelf deserialize(JsonElement element, Type type,
        JsonDeserializationContext ctx) throws JsonParseException {

        Gson g = new Gson();
        IShelf shelf = g.fromJson(element, Shelf.class);
        return shelf;
    }
}