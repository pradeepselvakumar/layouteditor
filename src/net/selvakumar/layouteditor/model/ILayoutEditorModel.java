package net.selvakumar.layouteditor.model;

import java.awt.Point;
import java.util.Collection;


public interface ILayoutEditorModel {
    public void setModelEventsListener(IModelEventsListener listener);

    public Collection<IShelf> getShelves();

    public IShelf addShelf(Point startPoint);

    public void addShelf(IShelf _shelf);

    public void addShelves(Collection<IShelf> shelves);

    public void setShelfLocation(IShelf s, Point point);

    public void removeShelf(IShelf s);

    public void removeShelves(Collection<IShelf> shelves);

    public LayoutModelSerializable getSerializableModel();

    public String getFileName();

    public void setFileName(String fileName);
}
