/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.model;

import java.io.IOException;

/**
 *
 * @author deeps
 */
public interface IGsonOperationsProvider {
    public void setFileOperationsProvider(IFileOperationsProvider provider);
    
    public void SaveAs(LayoutModelSerializable serializableModel, String fileName) throws IOException;

    public LayoutModelSerializable Open(String filePath) throws IOException;
}
