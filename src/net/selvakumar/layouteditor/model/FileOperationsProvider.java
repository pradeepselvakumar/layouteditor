/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.model;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author deeps
 */
class FileOperationsProvider implements IFileOperationsProvider {

    @Override
    public Writer getFileWriter(String fileName) throws IOException {
        return new FileWriter(fileName);
    }
    
}
