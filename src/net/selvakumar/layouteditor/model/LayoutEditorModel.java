package net.selvakumar.layouteditor.model;

import com.google.gson.annotations.SerializedName;
import java.awt.Point;
import java.util.Collection;
import java.util.HashSet;



public class LayoutEditorModel implements ILayoutEditorModel {

    static int getGridWidth() {
        return 50;
    }

    @SerializedName("shelves")
    private HashSet<IShelf> _shelves;

    private transient IModelEventsListener _modelEventsListener = null;
    private String _fileName = "";

    public LayoutEditorModel() {
        _shelves = new HashSet<IShelf>();
    }

    public LayoutEditorModel(IModelEventsListener modelEventsListener) {
        this();
        _modelEventsListener = modelEventsListener;
    }

    @Override
    public void setModelEventsListener(IModelEventsListener listener) {
        _modelEventsListener = listener;
    }

    @Override
    public Collection<IShelf> getShelves() {
        return _shelves;
    }

    @Override
    public IShelf addShelf(Point startPoint) {
        IShelf shelf = new Shelf(coerceShelfLocation(startPoint));
        addShelf(shelf);

        return shelf;
    }

    @Override
    public void addShelf(IShelf shelf) {
        if (_shelves.add(shelf)) {
            fireShelvesCollectionChangedEvent();
        }
    }

    @Override
    public void addShelves(Collection<IShelf> shelves) {
        if (_shelves.addAll(shelves)) {
            fireShelvesCollectionChangedEvent();
        }
    }

    @Override
    public void removeShelf(IShelf shelf) {
        if (_shelves.remove(shelf)) {
            fireShelvesCollectionChangedEvent();
        }
    }

    @Override
    public void removeShelves(Collection<IShelf> shelves) {
        _shelves.removeAll(shelves);
        fireShelvesCollectionChangedEvent();
    }

    private void fireShelvesCollectionChangedEvent() {
        if (_modelEventsListener != null)
            _modelEventsListener.shelvesCollectionChanged();
    }

    private Point coerceShelfLocation(Point location) {
        int gridCellWidth = getGridWidth();
        double horizontalAdjustment = location.x % gridCellWidth > (gridCellWidth / 2) ? 0.5 : 0;

        int newX = (int)((location.x / gridCellWidth + horizontalAdjustment) * gridCellWidth);
        int newY = (location.y / gridCellWidth) * gridCellWidth;

        return new Point(newX, newY);
    }

    @Override
    public void setShelfLocation(IShelf s, Point point) {
        s.setLocation(coerceShelfLocation(point));
    }

    @Override
    public LayoutModelSerializable getSerializableModel() {
        return new LayoutModelSerializable(this);
    }

    @Override
    public String getFileName() {
        return _fileName;
    }

    @Override
    public void setFileName(String fileName) {
        _fileName = fileName;
    }
}
class Shelf implements IShelf {
    private Point location;

    Shelf () {
        this(new Point());
    }

    Shelf(Point location) {
        this.location = location;
    }

    @Override
    public Point getLocation() {
        return location;
    }

    @Override
    public void setLocation(Point pt) {
        this.location = pt;
    }

}