/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor;

import net.selvakumar.layouteditor.presenter.LayoutEditorPresenter;
import net.selvakumar.layouteditor.presenter.ILayoutEditorPresenter;
import net.selvakumar.layouteditor.model.LayoutEditorModel;
import net.selvakumar.layouteditor.model.ILayoutEditorModel;
import net.selvakumar.layouteditor.view.LayoutEditorView;
import net.selvakumar.layouteditor.view.ILayoutEditorView;
import javax.swing.SwingUtilities;

/**
 *
 * @author Pradeep Selvakumar
 */
public class LayoutEditor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
				ILayoutEditorView view = new LayoutEditorView();
				ILayoutEditorModel model = new LayoutEditorModel();

				ILayoutEditorPresenter presenter = new LayoutEditorPresenter(view, model);
            }
        });
    }
}
