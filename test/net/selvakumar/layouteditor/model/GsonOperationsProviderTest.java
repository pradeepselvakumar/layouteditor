/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.model;

import java.awt.Point;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import org.hamcrest.Matchers;
import org.jmock.Expectations;
import org.jmock.Mockery;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deeps
 */
public class GsonOperationsProviderTest {
    public Mockery context = new Mockery();

    /**
     * Test of getJsonWriter method, of class GsonOperationsProvider.
     */
    @Test
    public void testSaveAs() throws Exception {

        final ILayoutEditorModel model = context.mock(ILayoutEditorModel.class);
        final IFileOperationsProvider fileOperationsMock = context.mock(IFileOperationsProvider.class);
        final String fileName = "/path/to/file";
        final Writer writer = new StringWriter();
        final Collection<IShelf> stubShelves = new ArrayList<IShelf>();
        final Point location = new Point(134, 53);
        final Shelf stubShelf = new Shelf(location);
        stubShelves.add(stubShelf);

        context.checking(new Expectations() {{
            oneOf(fileOperationsMock).getFileWriter(fileName); will (returnValue(writer));
            oneOf(model).getShelves(); will (returnValue(stubShelves));
        }});

        GsonOperationsProvider instance = new GsonOperationsProvider();
        instance.setFileOperationsProvider(fileOperationsMock);

        LayoutModelSerializable serializableModel = new LayoutModelSerializable(model);
        instance.SaveAs(serializableModel, fileName);

        // ideally, we should use a regex matcher, as described in http://stackoverflow.com/q/8505153/1775113
        assertThat(writer.toString(), Matchers.containsString(Integer.toString(location.y)));
        assertThat(writer.toString(), Matchers.containsString(Integer.toString(location.x)));

        context.assertIsSatisfied();
    }
}
