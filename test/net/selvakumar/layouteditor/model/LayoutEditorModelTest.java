/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.model;

import java.awt.Point;
import net.selvakumar.layouteditor.model.IModelEventsListener;
import net.selvakumar.layouteditor.model.IShelf;
import net.selvakumar.layouteditor.model.LayoutEditorModel;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author deeps
 */
public class LayoutEditorModelTest {
    public Mockery context = new Mockery();
    final IModelEventsListener _mockListener = context.mock(IModelEventsListener.class);
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testModelConstructorHasNoShelves() {
        LayoutEditorModel model = new LayoutEditorModel();
        assertEquals("Shelves list should be empty", 0, model.getShelves().size());
    }
    
    @Test
    public void testAddShelfFiresShelfAddedEvent() {
        final Point expectedLocation = new Point(0, 0);

        context.checking(new Expectations() {{
            oneOf (_mockListener).shelvesCollectionChanged();
        }});

        LayoutEditorModel model = new LayoutEditorModel();
        model.setModelEventsListener(_mockListener);
        
        Point location = new Point(10, 25);
        model.addShelf(location);
        assertEquals("There should be one shelf", 1, model.getShelves().size());
        
        context.assertIsSatisfied();
    }
    
    @Test
    public void testRemoveShelfFiresShelfRemovedEvent() {
        final Point expectedLocation = new Point(25, 0);
        
        context.checking(new Expectations() {{
            exactly(2).of(_mockListener).shelvesCollectionChanged();
        }});

        LayoutEditorModel model = new LayoutEditorModel(_mockListener);

        Point location = new Point(43, 25);
        IShelf newShelf = model.addShelf(location);

        assertEquals("There should be one shelf", 1, model.getShelves().size());

        model.removeShelf(newShelf);
        assertEquals("There should be zero shelves", 0, model.getShelves().size());

        model.removeShelf(newShelf); // cannot remove a shelf that is not in the list
                                     // the fact shelvesCollectionChanges is called only twice
                                     // proves this.

        context.assertIsSatisfied();
    }

    @Test
    public void testShelvesPositionAutomaticallyAlignsWithGrid() {
        final Point expectedLocation = new Point (25, 50);
        
         context.checking(new Expectations() {{
            oneOf (_mockListener).shelvesCollectionChanged();
        }});

        LayoutEditorModel model = new LayoutEditorModel(_mockListener);

        Point shelfLocation = new Point(43, 57);
        IShelf newShelf = model.addShelf(shelfLocation);

        assertEquals("There should be one shelf", 1, model.getShelves().size());
        assertEquals("The shelf location should be valid", expectedLocation, newShelf.getLocation());
        
        context.assertIsSatisfied();
    }
}

class ShelfMatcher extends TypeSafeMatcher<IShelf> {
    IShelf shelf;
    IShelf otherShelf;
    public ShelfMatcher(IShelf shelf) {
        this.shelf = shelf;
    }
    
    public boolean matchesSafely(IShelf otherShelf) {
        this.otherShelf = otherShelf;
        Point location = shelf.getLocation();
        Point otherLocation = otherShelf.getLocation();
        
        return location.x == otherLocation.x && location.y == otherLocation.y;
        
    }

    @Override
    public void describeTo(Description description) {
        String desc = String.format("Shelf locations. Expected: %s; Actual: %s)", shelf.getLocation().toString(), otherShelf.getLocation().toString());
        description.appendText(desc);
    }
}
