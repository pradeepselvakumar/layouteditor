/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.selvakumar.layouteditor.presenter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JOptionPane;
import net.selvakumar.layouteditor.model.IGsonOperationsProvider;
import net.selvakumar.layouteditor.model.ILayoutEditorModel;
import net.selvakumar.layouteditor.model.IShelf;
import net.selvakumar.layouteditor.model.LayoutModelSerializable;
import net.selvakumar.layouteditor.view.ILayoutEditorView;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pselvakumar
 */
public class LayoutPresenterSerializationTests {
    public Mockery _context = new Mockery();

    final ILayoutEditorView _mockView = _context.mock(ILayoutEditorView.class);
    final ILayoutEditorModel _mockModel = _context.mock(ILayoutEditorModel.class);
    Collection<IShelf> _stubShelves = new ArrayList<>();
    private LayoutModelSerializable _serializableModel;

    @Before
    public void setUp() {
        _context.checking(new Expectations() {{
            oneOf(_mockModel).getShelves(); will(returnValue(_stubShelves));
        }});

         _serializableModel = new LayoutModelSerializable(_mockModel);
    }

    @Test
    public void fileSaveDoesNothingIfFileDialogDoesNotReturnFileName() {

        final IGsonOperationsProvider mockProvider = _context.mock(IGsonOperationsProvider.class);

        _context.checking(new Expectations() {{
            oneOf(_mockModel).getFileName(); will(returnValue(""));
            oneOf(_mockView).showFileSaveDialog(); will(returnValue(""));

            ignoring(_mockView);
            ignoring(_mockModel);

            never(mockProvider);
        }});

        ILayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.setGsonOperationsProvider(mockProvider);

        presenter.fileSave();

        _context.assertIsSatisfied();
    }

    @Test
    public void fileSaveDelegatesToGsonOpsProvider() throws IOException {
        final IGsonOperationsProvider mockProvider = _context.mock(IGsonOperationsProvider.class);
        final String filePath = "/path/to/file";

        _context.checking(new Expectations() {{
            oneOf(_mockView).showFileSaveDialog(); will(returnValue(filePath));

            oneOf(_mockModel).getFileName(); will(returnValue(""));
            oneOf(_mockModel).getSerializableModel(); will(returnValue(_serializableModel));
            oneOf(mockProvider).SaveAs(_serializableModel, filePath);

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        ILayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.setGsonOperationsProvider(mockProvider);

        presenter.fileSave();

        _context.assertIsSatisfied();
    }

    @Test
    public void fileSaveDisplaysMessageOnException() throws IOException {
        final IGsonOperationsProvider mockProvider = _context.mock(IGsonOperationsProvider.class);
        final String filePath = "/path/to/file";

        final IOException stubException = new IOException();

        _context.checking(new Expectations() {{
            oneOf(_mockView).showFileSaveDialog(); will(returnValue(filePath));

            oneOf(_mockModel).getFileName(); will(returnValue(""));
            oneOf(_mockModel).getSerializableModel(); will(returnValue(_serializableModel));

            oneOf(_mockView).showErrorDialog(LayoutEditorPresenter.EditorMessages.ERROR_SAVING_FILE, stubException);

            oneOf(mockProvider).SaveAs(_serializableModel, filePath); will(throwException(stubException));

            ignoring(_mockView);
            ignoring(_mockModel);

        }});

        ILayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.setGsonOperationsProvider(mockProvider);

        presenter.fileSave();

        _context.assertIsSatisfied();
    }

    @Test
    public void fileOpenWithNoFileNameDoesNothing() {
        _context.checking(new Expectations() {{
            oneOf(_mockView).showFileOpenDialog(); will(returnValue(""));

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        ILayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.fileOpen();

        _context.assertIsSatisfied();
    }

    @Test
    public void fileOpenWithValidFileName() throws IOException {
        final IGsonOperationsProvider mockProvider = _context.mock(IGsonOperationsProvider.class);
        final String filePath = "/path/to/file";

        _context.checking(new Expectations() {{

            oneOf(_mockView).showFileOpenDialog(); will(returnValue(filePath));
            oneOf(mockProvider).Open(filePath); will(returnValue(_serializableModel));

            oneOf(_mockModel).addShelves(_stubShelves);
            oneOf(_mockModel).setFileName(filePath);
            oneOf(_mockView).setWindowTitle(filePath);

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        ILayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.setGsonOperationsProvider(mockProvider);

        presenter.fileOpen();

        _context.assertIsSatisfied();
    }

    @Test
    public void fileOpenWithUndoableActions() throws IOException {
        final IGsonOperationsProvider mockProvider = _context.mock(IGsonOperationsProvider.class);
        final String fileName = "/path/to/file";
        
        _context.checking(new Expectations() {{
            oneOf(_mockView).showSaveDialog(LayoutEditorPresenter.EditorMessages.SHOULD_SAVE);will(returnValue(JOptionPane.NO_OPTION));
            oneOf(_mockView).showFileOpenDialog(); will(returnValue(fileName));
            oneOf(mockProvider).Open(fileName); will(returnValue(_serializableModel));
            
            oneOf(_mockView).showEditor();
            oneOf(_mockView).setViewEventsListener(with(any(LayoutEditorPresenter.class)));
            oneOf(_mockView).setWindowTitle(fileName);
            oneOf(_mockModel).setFileName(fileName);
            oneOf(_mockView).redraw();
            
            ignoring(_mockModel);
            ignoring(mockProvider);
        }});

        ExtendedUndoManager stubUndoManager = new ExtendedUndoManager();

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.setGsonOperationsProvider(mockProvider);
        
        presenter.setUndoManager(stubUndoManager);
        stubUndoManager.addEdit(new MoveSelectedShelvesCommand(presenter, _mockModel));

        presenter.fileOpen();

        assertFalse("The undo stack should be clear after a file open", stubUndoManager.canUndo());
        _context.assertIsSatisfied();
    }
    
    @Test
    public void fileOpenAfterFileSaveShouldNotAskToSave() {
    
        final IGsonOperationsProvider mockProvider = _context.mock(IGsonOperationsProvider.class);
        
        _context.checking(new Expectations() {{
            oneOf(_mockModel).getFileName(); will(returnValue("some/file/name"));
            never(_mockView).showSaveDialog(LayoutEditorPresenter.EditorMessages.SHOULD_SAVE);will(returnValue(JOptionPane.CANCEL_OPTION));
            ignoring(_mockModel);
            ignoring(mockProvider);
            oneOf(_mockView).showEditor();
            oneOf(_mockView).showFileOpenDialog();
            oneOf(_mockView).setViewEventsListener(with(any(LayoutEditorPresenter.class)));
            
        }});

        ExtendedUndoManager stubUndoManager = new ExtendedUndoManager();

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        
        presenter.setGsonOperationsProvider(mockProvider);
        presenter.setUndoManager(stubUndoManager);
        stubUndoManager.addEdit(new MoveSelectedShelvesCommand(presenter, _mockModel));

        presenter.fileSave();
        presenter.fileOpen();

        _context.assertIsSatisfied();
    }
}
