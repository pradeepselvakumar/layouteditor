package net.selvakumar.layouteditor.presenter;

import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import javax.swing.JFrame;
import net.selvakumar.layouteditor.model.ILayoutEditorModel;
import net.selvakumar.layouteditor.model.IModelEventsListener;
import net.selvakumar.layouteditor.model.IShelf;
import net.selvakumar.layouteditor.view.ILayoutEditorView;
import net.selvakumar.layouteditor.view.IViewEventsListener;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class LayoutPresenterMouseTest {
    public Mockery _context = new Mockery();

    final ILayoutEditorView _mockView = _context.mock(ILayoutEditorView.class);
    final ILayoutEditorModel _mockModel = _context.mock(ILayoutEditorModel.class);

    MouseEvent _mouseEvent;
    Collection<IShelf> _stubShelves = new ArrayList<IShelf>();

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
          _context.checking(new Expectations() {{
            allowing (_mockModel).getShelves(); will(returnValue(_stubShelves));
        }});
    }

    @After
    public void tearDown() {
      _stubShelves.clear();
    }

    @Test
    public void testConstructor() {
        _context.checking(new Expectations() {{
            oneOf (_mockView).showEditor();
            oneOf (_mockView).setViewEventsListener(with(any(IViewEventsListener.class)));
            oneOf (_mockModel).setModelEventsListener(with(any(IModelEventsListener.class)));
        }});

        new LayoutEditorPresenter(_mockView, _mockModel);

        _context.assertIsSatisfied();
    }

    @Test
    public void testNewShelf() {
        _context.checking(new Expectations() {{
            oneOf(_mockView).showStatus(LayoutEditorPresenter.StatusMessages.CLICK_TO_ADD_SHELF);
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);

        assertEquals("Editor is in Clear status", LayoutEditorPresenter.EditorStatuses.Normal, presenter.getCurrentEditorStatus());

        presenter.newShelf();

        _context.assertIsSatisfied();
        assertEquals("Editor is ready to AddShelf", LayoutEditorPresenter.EditorStatuses.AddShelf, presenter.getCurrentEditorStatus());
    }

    @Test
    public void testMouseEnterCallsShowDefaultCursor() {
        _context.checking(new Expectations() {{
            oneOf (_mockView).showDefaultCursor();
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);

        fireMouseEntered(presenter);

        _context.assertIsSatisfied();
    }

    @Test
    public void testMouseExitedCallsShowDefaultCursor() {
        _context.checking(new Expectations() {{
            oneOf (_mockView).showDefaultCursor();
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);

        fireMouseExited(presenter);
        _context.assertIsSatisfied();
    }

    @Test
    public void testMouseEnterAfterNewShelfCallsShowCrosshair() {
        _context.checking(new Expectations() {{
            oneOf (_mockView).showCrosshair();
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.newShelf();
        fireMouseEntered(presenter);

        _context.assertIsSatisfied();
    }

    @Test
    public void testMouseExitedAfterNewShelfCallsShowDefaultCursor() {
        _context.checking(new Expectations() {{
            oneOf (_mockView).showDefaultCursor();
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.newShelf();
        fireMouseExited(presenter);

        _context.assertIsSatisfied();
    }

    @Test
    public void testHandleMousePressedDefaultBehavior() {
        _context.checking(new Expectations() {{
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);

        fireMousePressed(presenter, new Point());
        assertEquals("Should be in normal mode", presenter.getCurrentEditorStatus(), LayoutEditorPresenter.EditorStatuses.Normal);

        _context.assertIsSatisfied();
    }

    @Test
    public void testNewShelfWithMouseClick() {
        final Point location = new Point(10, 15);

         _context.checking(new Expectations() {{
            oneOf(_mockModel).addShelf(location);

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.newShelf();

        assertEquals("Status should be AddShelf", presenter.getCurrentEditorStatus(), LayoutEditorPresenter.EditorStatuses.AddShelf);

        fireMousePressed(presenter, location);
        fireMouseReleased(presenter, location);

        assertEquals("Status should still be AddShelf", presenter.getCurrentEditorStatus(), LayoutEditorPresenter.EditorStatuses.AddShelf);

        _context.assertIsSatisfied();
    }

    @Test
    public void testHandleMouseOverAShelfChangesCursor() {
        final IShelf shelfStub = _context.mock(IShelf.class);
        _stubShelves.add(shelfStub);

        _context.checking(new Expectations() {{
            oneOf(_mockView).showDragDropCursor();
            oneOf(shelfStub).getLocation(); will (returnValue(new Point(100, 0)));

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        Point location = new Point(110, 45);

        fireMouseMoved(presenter, location);

        _context.assertIsSatisfied();
    }

    @Test
    public void testMouseDownOnShelfSelectsIt()
    {
        final IShelf shelfStub = _context.mock(IShelf.class);
        _stubShelves.add(shelfStub);

        _context.checking(new Expectations() {{
            exactly(2).of(shelfStub).getLocation(); will (returnValue(new Point(100, 0)));
            oneOf (_mockView).setSelectedShelves(with(any(HashSet.class)));
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        Point location = new Point(110, 45);

        fireMousePressed(presenter, location);
        assertEquals("There should be one shelf in the selectedShelves list", 1, presenter.getSelectedShelves().size());
        assertEquals("The shelf did not match", shelfStub, presenter.getSelectedShelves().toArray()[0]);

        // clicking on the shelf again should not re-select it
        fireMousePressed(presenter, location);

        _context.assertIsSatisfied();
    }

    @Test
    public void testMouseDownOnGridDeselectsShelves() {
        final IShelf shelfStub = _context.mock(IShelf.class);
        _stubShelves.add(shelfStub);

        _context.checking(new Expectations() {{
            exactly(2).of(shelfStub).getLocation(); will (returnValue(new Point(100, 0)));
            oneOf (_mockView).setSelectedShelves(with(any(HashSet.class)));
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        Point location = new Point(110, 45);

        fireMousePressed(presenter, location);
        assertEquals("There should be one shelf in the selectedShelves list", 1, presenter.getSelectedShelves().size());
        assertEquals("The shelf did not match", shelfStub, presenter.getSelectedShelves().toArray()[0]);

        fireMousePressed(presenter, new Point(10, 5));
        assertEquals("There should be 0 shelves in the selectedShelves list", 0, presenter.getSelectedShelves().size());

        _context.assertIsSatisfied();
    }

    @Test
    public void testCtrlClickCausesDiscreteSelection() {
         final IShelf shelf1Stub = _context.mock(IShelf.class, "first");
         final IShelf shelf2Stub = _context.mock(IShelf.class, "second");
        _stubShelves.add(shelf1Stub);
        _stubShelves.add(shelf2Stub);

        _context.checking(new Expectations() {{
            atLeast(1).of(shelf1Stub).getLocation(); will (returnValue(new Point(100, 0)));
            atLeast(1).of(shelf2Stub).getLocation(); will (returnValue(new Point(200, 0)));
            oneOf (_mockView).setSelectedShelves(with(any(HashSet.class)));

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        Point location = new Point(110, 45);

        fireMousePressed(presenter, location);
        assertEquals("There should be one shelf in the selectedShelves list", 1, presenter.getSelectedShelves().size());
        assertEquals("The shelf did not match", shelf1Stub, presenter.getSelectedShelves().toArray()[0]);

        // ctrl-clicking on the second shelf should select the second shelf as well
        fireMousePressedWithCtrl(presenter, new Point(210, 0));
        assertEquals("There should be two shelves in the selectedShelves list", 2, presenter.getSelectedShelves().size());

        _context.assertIsSatisfied();
    }

    @Test
    public void testCtrlClickCausesDeselectionOfSelectedShelf()    {
        final IShelf shelf1Stub = _context.mock(IShelf.class);
        _stubShelves.add(shelf1Stub);

        _context.checking(new Expectations() {{
            atLeast(1).of(shelf1Stub).getLocation(); will (returnValue(new Point(100, 0)));
            oneOf (_mockView).setSelectedShelves(with(any(HashSet.class)));

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        Point location = new Point(110, 45);

        fireMousePressed(presenter, location);
        assertEquals("There should be one shelf in the selectedShelves list", 1, presenter.getSelectedShelves().size());
        assertEquals("The shelf did not match", shelf1Stub, presenter.getSelectedShelves().toArray()[0]);

        // ctrl-clicking on the second shelf should select the second shelf as well
        fireMousePressedWithCtrl(presenter, new Point(110, 45));
        assertEquals("There should be 0 shelves in the selectedShelves list", 0, presenter.getSelectedShelves().size());

        _context.assertIsSatisfied();
    }

    @Test
    public void testDragShelfMovesIt() {
        final IShelf shelf1Stub = _context.mock(IShelf.class);
        _stubShelves.add(shelf1Stub);
        final Point location = new Point(130, 55);

        _context.checking(new Expectations() {{
            atLeast(1).of(shelf1Stub).getLocation(); will (returnValue(new Point(100, 0)));
            oneOf (shelf1Stub).setLocation(location);
            oneOf (_mockView).setShelves(_stubShelves);
            oneOf (_mockView).setSelectedShelves(with(any(HashSet.class)));
            exactly(2).of(_mockView).redraw();

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);

        fireMousePressed(presenter, new Point(100, 0));
        assertEquals("There should be one shelf in the selectedShelves list", 1, presenter.getSelectedShelves().size());
        assertEquals("The shelf did not match", shelf1Stub, presenter.getSelectedShelves().toArray()[0]);

        // dragging the mouse should move the shelf
        fireMouseDragged(presenter, location);

        assertEquals("There should be 1 shelves in the selectedShelves list", 1, presenter.getSelectedShelves().size());

        _context.assertIsSatisfied();
    }

    @Test
    public void dragShelfAndMouseUpCoercesDestination() {
        final IShelf shelfStub = _context.mock(IShelf.class);
        _stubShelves.add(shelfStub);
        final Point location = new Point(130, 55);

        _context.checking(new Expectations() {{
            exactly(3).of(shelfStub).getLocation(); will (returnValue(new Point(100, 0)));
            exactly(2).of(shelfStub).getLocation(); will (returnValue(location));

            oneOf (shelfStub).setLocation(location);
            oneOf (_mockModel).setShelfLocation(shelfStub, location);
            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);

        fireMousePressed(presenter, new Point(100, 0));
        assertEquals("There should be one shelf in the selectedShelves list", 1, presenter.getSelectedShelves().size());
        assertEquals("The shelf did not match", shelfStub, presenter.getSelectedShelves().toArray()[0]);

        // dragging the mouse should move the shelf
        fireMouseDragged(presenter, location);
        fireMouseReleased(presenter, location);
        assertEquals("There should be 1 shelves in the selectedShelves list", 1, presenter.getSelectedShelves().size());

        _context.assertIsSatisfied();
    }

    @Test
    public void HittingDeleteDeletesSelectedShelves() {
        final IShelf shelfStub = _context.mock(IShelf.class);
        _stubShelves.add(shelfStub);
        final Point location = new Point(130, 55);

        _context.checking(new Expectations() {{
            allowing(shelfStub).getLocation(); will (returnValue(location));

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);

        fireMousePressedWithCtrl(presenter, location);
        assertEquals("There should be one shelf in the selectedShelves list", 1, presenter.getSelectedShelves().size());

        fireDeleteKeyPressed(presenter);
        assertEquals("There should be 0 shelves in the selectedShelves list", 0, presenter.getSelectedShelves().size());

        _context.assertIsSatisfied();
    }

    @Test
    public void addingAShelfIsUndoable() {
        final Point location = new Point(10, 15);

        final IShelf shelfStub = _context.mock(IShelf.class);

         _context.checking(new Expectations() {{
            oneOf(_mockModel).addShelf(location); will(returnValue(shelfStub));
            oneOf(_mockModel).removeShelf(shelfStub);

            ignoring(_mockView);
            ignoring(_mockModel);
        }});

        LayoutEditorPresenter presenter = new LayoutEditorPresenter(_mockView, _mockModel);
        presenter.newShelf();
        fireMousePressed(presenter, location);
        fireMouseReleased(presenter, location);
        presenter.undo();

        _context.assertIsSatisfied();
    }

    MouseEvent getMouseEventStub() {
        return getMouseEventStub(new Point(), 0);
    }

    MouseEvent getMouseEventStub(Point point, int modifiers) {
        if (_mouseEvent == null || _mouseEvent.getPoint() != point) {
            JFrame frame = new JFrame();
            _mouseEvent = new MouseEvent(frame, 0 , System.currentTimeMillis(), modifiers, point.x, point.y, 1, 4, 1, true, 0);
        }

        return _mouseEvent;
    }

    KeyEvent getKeyEventStub(int keyCode) {
        JFrame frame = new JFrame();
        return new KeyEvent(frame, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, keyCode, KeyEvent.CHAR_UNDEFINED);
    }

    private void fireMouseEntered(LayoutEditorPresenter presenter) {
       presenter.getMouseHandler().mouseEntered(getMouseEventStub());
    }

    private void fireMouseExited(LayoutEditorPresenter presenter) {
        presenter.getMouseHandler().mouseExited(getMouseEventStub());
    }

    private void fireMouseMoved(LayoutEditorPresenter presenter, Point endPoint) {
        presenter.getMouseHandler().mouseMoved(getMouseEventStub(endPoint, 0));
    }

    private void fireMousePressed(LayoutEditorPresenter presenter, Point endPoint) {
        presenter.getMouseHandler().mousePressed(getMouseEventStub(endPoint, 0));
    }

    private void fireMousePressedWithCtrl(LayoutEditorPresenter presenter, Point endPoint) {
        presenter.getMouseHandler().mousePressed(getMouseEventStub(endPoint, InputEvent.CTRL_DOWN_MASK));
    }

    private void fireMouseDragged(LayoutEditorPresenter presenter, Point endPoint) {
        presenter.getMouseHandler().mouseDragged(getMouseEventStub(endPoint, 0));
    }

    private void fireMouseReleased(LayoutEditorPresenter presenter, Point location) {
        presenter.getMouseHandler().mouseReleased(getMouseEventStub(location, 0));
    }

    private void fireDeleteKeyPressed(LayoutEditorPresenter presenter) {
        presenter.getKeyEventsHandler().dispatchKeyEvent(getKeyEventStub(KeyEvent.VK_DELETE));
    }
}
